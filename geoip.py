import pygeoip
from os import path
GeoIPAgent = pygeoip.GeoIP(path.join(path.dirname(__file__), 'GeoLiteCity.dat'))

def location(ip):
    record = GeoIPAgent.record_by_addr(ip)
    return {'lat': record['latitude'], 'lng': record['longitude']} if 'latitude' in record and 'longitude' in record else None

record = GeoIPAgent.record_by_addr
