import cherrypy

INFO = 0
ERROR = 1
SUCCESS = 2

def send(msg, kind=INFO):
	if 'inbox' not in cherrypy.session: cherrypy.session['inbox'] = list()
	cherrypy.session['inbox'].append({'msg': msg, 'kind': kind})

def get():
	inbox = cherrypy.session.get('inbox', {})
	while len(inbox) > 0: yield inbox.pop()