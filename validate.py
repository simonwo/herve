# Form validation techniques
import formencode
import cherrypy
import messaging
from os.path import join, abspath
from furl import furl as URL

def validate_form(form, onerror=None):
    try: 
        fields = dict([(k,v) for k,v in cherrypy.request.params.items() if k in form.fields])
        fields.update(form.to_python(fields))
        cherrypy.request.params.update(fields)
    except formencode.api.Invalid as error: 
        messaging.send(str(error), messaging.ERROR)
        path = URL(abspath(join(cherrypy.request.path_info, onerror)) if onerror is not None else cherrypy.request.path_info).add(fields)
        raise cherrypy.HTTPRedirect(str(path)) #CHECK SECURITY
cherrypy.tools.validate_form = cherrypy.Tool('before_handler', validate_form)

def paramfilter(*args, **kwargs):
    return dict( [(k, kwargs.get(k)) for k in args] )

from formencode.validators import DateValidator
from formencode.schema import SimpleFormValidator
from datetime import datetime

def DatetimeValidator(date_field, time_field, *args, **kwargs):
    def validate(form_values, state, validator):
        DateValidator(*args, **kwargs).to_python(datetime.combine(form_values[date_field], form_values[time_field]))
    return SimpleFormValidator(validate)
