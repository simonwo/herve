#!/bin/sh
set -e

sudo apt-get -y install python2.7 libxml2-dev libxslt-dev python-pip python-dev sqlite3 libz-dev
sudo pip install pygeoip
sudo pip install python-google-places
sudo pip install lxml
sudo pip install sqlalchemy
sudo pip install cherrypy
sudo pip install furl
sudo pip install facebook-sdk
sudo pip install git+http://github.com/formencode/formencode 
sudo pip install routes

wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
gzip -q -d GeoLiteCity.dat.gz

mkdir run/ 2>/dev/null
mkdir run/placeimg 2>/dev/null
echo """.tables
.exit""" | sqlite3 run/sqlite.db
