# Form validation techniques
import formencode
import cherrypy
import messaging
import fragment
import uuid
from os.path import join, abspath
from furl import furl as URL

def require_login():
	'''Require the user to be logged in'''
	if "user" not in cherrypy.session:
		raise cherrypy.HTTPError(401)
cherrypy.tools.require_login = cherrypy.Tool('before_handler', require_login)

# Pages with this decorator require a CSRF token to match
# TODO - put in some sort of expiry system?
def require_csrf():
        '''Require a CSRF token to be present and match'''
	if 'csrf' not in cherrypy.request.params or 'csrf' not in cherrypy.session or cherrypy.request.params['csrf'] not in cherrypy.session.get('csrf'):
		messaging.send("CSRF check failed. If you intended to do something, try it again.", messaging.ERROR)
		raise cherrypy.HTTPError(403)
	else:
		del cherrypy.request.params['csrf']
cherrypy.tools.require_csrf = cherrypy.Tool('before_handler', require_csrf)

def write_csrf(link=None):
	csrf = cherrypy.session['csrf'] = cherrypy.session.get('csrf') or generate_csrf()
	return URL(link).add("csrf=%s" % csrf) if link else csrf
fragment.addtoscope(csrf=write_csrf)

def generate_csrf():
	return str(uuid.uuid4())
