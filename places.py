import cherrypy
from googleplaces import GooglePlaces, types
import database
import database.place
from database import Place, Image
from database.reservation import Reservation
from sqlalchemy import or_ 
from os import path
import mimetypes
from urllib import unquote_plus
import messaging
from datetime import datetime

__GOOGLE_API_KEY = "AIzaSyAgXZfEvHYF6dYbEkyYl9s49U0XieaRU0Q"
#__GOOGLE_API_KEY = "AIzaSyAFRXy3yobtf0jTfTTFtq9KCsFUHHExXbk"
_agent = GooglePlaces(__GOOGLE_API_KEY)

# this is naive
def search(query, location=None):
    # Get google search result for this query
    result = _agent.text_search(query=query, types=[types.TYPE_RESTAURANT], lat_lng=location)
    filters = map(lambda p: Place.id == p.id, result.places)

    # Find matching database objects
    db = database.session()
    m = db.query(Place).filter(or_(*filters))
    matches = dict(zip([p.id for p in m], m))

    for p in result.places:
        if p.id in matches:
            # Place already in the db - remove and return
            # later -> check if it is up to date?
            yield matches[p.id]
        else:
            # Lookup details for remaining places and insert them
            p.get_details()
            new_place = database.place.BuildFromGooglePlace(p)
            db.add(new_place)

            # Add slug
            new_place.slug = new_place.generate_slug()

            # Find and retrieve largest photo
            if len(p.photos) > 0:
                photo = max([(ph.orig_width * ph.orig_height, ph) for ph in p.photos])[1]
                photo.get(photo.orig_width, photo.orig_height)

                # Choose a unique filename and write file
                imgdir = cherrypy.request.app.config['paths']['imgdir']
                filename = Image.generate_filename(new_place.id, mimetypes.guess_extension(photo.mimetype) or path.splitext(photo.filename)[1], imgdir)
                with open(path.join(imgdir, filename), 'w') as f:
                    f.write(photo.data)

                new_place.images.append(Image(filename=filename, width=photo.orig_width, 
                                        height=photo.orig_height, mimetype=photo.mimetype))

            db.commit()
            yield new_place
    

from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from lxml import html
import fragment
from formencode import Schema
from formencode.validators import DateConverter, TimeConverter, UnicodeString, Int
from validate import DatetimeValidator, paramfilter

class PlaceViewer():

    def find(self, slug):
        try: return Place.find_by_slug(unquote_plus(slug)).one()
        except NoResultFound: raise cherrypy.HTTPError(404)

    @cherrypy.expose
    def index(self, slug=None):
        # Find the place
        place = self.find(slug)

        # Present the data
        page, = fragment.require('place_page')
        return html.tostring( page(place=place) )

    @cherrypy.tools.require_csrf()
    @cherrypy.tools.require_login()
    @cherrypy.expose
    def subscribe(self, slug=None):
        # Find the place
        place = self.find(slug)

        # Check the subscription
        db = database.session()
        if cherrypy.session.get("user") in place.subscribed:
            messaging.send("You're already subscribed to this place.", messaging.INFO)
            raise cherrypy.HTTPRedirect("/p/%s/" % place.slug_url)

        # Add the subscription
        db.add(place)
        place.subscribed.append(cherrypy.session.get("user"))
        db.commit()

        messaging.send("You're now subscribed to reservations for %s!" % place.name, messaging.SUCCESS)
        raise cherrypy.HTTPRedirect("/p/%s/" % place.slug_url)

    @cherrypy.tools.require_csrf()
    @cherrypy.tools.require_login()
    @cherrypy.expose
    def unsubscribe(self, slug=None):
        # Find the place
        place = self.find(slug)

        # Check the subscription
        db = database.session()
        if cherrypy.session.get("user") not in place.subscribed:
            messaging.send("You're not subscribed to this place.", messaging.INFO)
            raise cherrypy.HTTPRedirect("/p/%s/" % place.slug_url)

        # Remove the subscription
        db.add(place)
        place.subscribed.remove(cherrypy.session.get("user"))
        db.commit()

        messaging.send("You're now unsubscribed to reservations for %s." % place.name, messaging.SUCCESS)
        raise cherrypy.HTTPRedirect("/p/%s/" % place.slug_url)

    @cherrypy.tools.require_login()
    @cherrypy.expose
    def offer(self, slug, **kwargs):
        '''Form page for offering reservations'''
        page, form = fragment.require('basepage', 'reservation_form')
        kwargs = paramfilter('date', 'timeval', 'people', 'bookingref', **kwargs)
        return html.tostring( page( form(action='/p/%s/offer_process' % slug, **kwargs), pagetitle="Offer Reservation") )	

    class offer_form(Schema):    
        date = DateConverter(month_style='yyyy/mm/dd')
        timeval = TimeConverter(use_datetime=True)
        people = Int()
        bookingref = UnicodeString(min=2)
        chained_validators = [ DatetimeValidator('date', 'timeval', after_now=True) ]

    @cherrypy.tools.validate_form(form=offer_form, onerror='../offer/')
    @cherrypy.tools.require_csrf()
    @cherrypy.tools.require_login()
    @cherrypy.expose
    def offer_process(self, slug, date, timeval, people, bookingref):
        
        # Find the place
        place = self.find(slug)

        # Create the reservation in the database
        db = database.session()
        db.add(place)
        place.reservations.append( Reservation(datetime=datetime.combine(date, timeval), people=people, bookingref=bookingref) )
        db.commit()

        # Message and redirect
        messaging.send("Yes!! Your reservation has been offered! Thanks :-)", messaging.SUCCESS)
        raise cherrypy.HTTPRedirect("/p/%s/" % place.slug_url)
