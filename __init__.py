import cherrypy
import securis 
import places
import fragment
import geoip
from lxml import html
import database # don't delete - ensures tables exist!
from os import path, getcwd
import messaging

ROOT = path.abspath(path.dirname(__file__))
print "root dir is", ROOT

fragment.loadall(path.join(path.dirname(__file__), 'fragments/'))
fragment.addtoscope(inbox=lambda: [m['msg'] for m in messaging.get()])
fragment.addtoscope(user=lambda: cherrypy.session.get("user"))

# Database teardown tool
def CloseSession():
	database.session.remove()
cherrypy.tools.autoclosedb = cherrypy.Tool('on_end_request', CloseSession)

# Logged in user rebind
def Rebind(objects):
	database.session().add_all([cherrypy.session.get(o) for o in objects if o in cherrypy.session])
cherrypy.tools.rebind = cherrypy.Tool('before_handler', Rebind)

class HerveProject:

	@cherrypy.expose
	def index(self):
		print cherrypy.config
		text = "You are logged in as " + cherrypy.session.get("user").name if "user" in cherrypy.session else "You are not logged in."
		return text + "\n".join([m['msg'] for m in messaging.get()])

	@cherrypy.expose
	def search(self, q=None):
		if q:
			page, search_item = fragment.require('search_results_page', 'search_item')
			items = [search_item(place=p) for p in places.search(q, location=cherrypy.session.get('location', None))] #SECURITY?
			#except Exception: raise cherrypy.HTTPError()
			return html.tostring( page(*items, q=q) )
		else:
			page, = fragment.require('search_page')
			return html.tostring( page() )

# If the user does not have a location set, find one
def IPLocator():
	if not cherrypy.session.get('location', False):
		cherrypy.session['location'] = geoip.location(cherrypy.request.remote.ip)
cherrypy.tools.iplocator = cherrypy.Tool('before_handler', IPLocator)

from inspect import getmembers, ismethod
def makeroutes(dispatcher, base, obj, trailing_slash=True, **kwargs):
    # Make base empty if it is just a slash
    base = base if base != "/" else ""

    # Find all the exposed member functions
    routes = dict([("/" + action, action) for action, func in getmembers(obj, predicate=lambda a: ismethod(a) and hasattr(a, 'exposed'))])
    if '/index' in routes: routes[''] = routes['/index']
    if '/default' in routes: routes[''] = routes['/default']

    # Add trailing slash routes
    if trailing_slash: routes.update([(k + '/', v) for k, v in routes.items()])
    print routes
    # Actually map the routes
    for action in routes:
        dispatcher.connect(obj.__class__.__name__ + action, base + action, controller=obj, action=routes[action], **kwargs)

# Automatically set up routes
d = cherrypy.dispatch.RoutesDispatcher()
makeroutes(d, "/", HerveProject())

from places import PlaceViewer
makeroutes(d, "/p/{slug}", PlaceViewer())

from facebook_utils import FacebookLogin
makeroutes(d, '/login/facebook', FacebookLogin())

# Set up server configuration, such as IP:port config
cherrypy.config.update(path.join(ROOT, 'server.config'))

# Set up application configuration and mount app
app_config = {'/': {'request.dispatch': d}}
cherrypy._cpconfig.merge(app_config, path.join(ROOT, 'herve.config'))
cherrypy.tree.mount(None, config=app_config)
