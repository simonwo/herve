import cherrypy
import uuid
from furl import furl as URL
from urllib2 import urlopen
from datetime import datetime, timedelta
from database import session
from database.user import User, LinkedAccount, AlreadyLinked
import facebook
import messaging
from functools import partial
from sqlalchemy import or_

APP_ID = "300958759977868"
APP_SECRET = "691beacc7e0595f4ca570eb94c89f34d"
REDIRECT_URI = 'http://162.13.42.104:8080/login/facebook/process/'

# Find all a given users friends
def FindFacebookFriends(handle, access_token):
	fb = facebook.GraphAPI(access_token)
	friends = list()
	__after_id = offset = 0
	limit = 5000
	while offset is not None:
		result = fb.get_connections(handle, 'friends', __after_id=__after_id, limit=limit, offset=offset)
		friends.extend(result['data'])
		if 'paging' in result and 'next' in result['paging']:
			paging = URL(result['paging']['next']).query.params
			__after_id = paging.get('__after_id')
			offset = paging.get('offset')
			limit = paging.get('limit')
		else: break

	return session().query(User).join(LinkedAccount).filter(or_(*[LinkedAccount.handle == f['id'] for f in friends]))

# Pages for logging people in via Facebook / linking Facebook accounts
class FacebookLogin:
	@cherrypy.expose
	def index(self):
		state = str(uuid.uuid4())
		cherrypy.session['facebook_login_state'] = state
		raise cherrypy.HTTPRedirect(facebook.auth_url(APP_ID, REDIRECT_URI, perms=['email'], state=state), 307)

	@cherrypy.expose
	def process(self, code=None, state=None, error_reason=None, error=None, error_description=None):
		if code and state and state == cherrypy.session['facebook_login_state']:
			# Look's like we have some valid stuff.
			try:
				auth = facebook.get_access_token_from_code(code, REDIRECT_URI, APP_ID, APP_SECRET)
				account = LinkedAccount(source='facebook',
										token=auth['access_token'],
										token_expires=datetime.now() + timedelta(seconds=int(auth['expires'])))

			except facebook.GraphAPIError as e: 
				messaging.send("Sorry, Facebook didn't play ball. You may be able to try again.", messaging.ERROR)
				raise cherrypy.HTTPRedirect(cherrypy.session.get('redirect', '/'), 307)

			# Fill in missing info about the user
			fb = facebook.GraphAPI(account.token)
			fbdetails = fb.get_object('me')
			account.handle = fbdetails['id']

			# Is the user logged in or not?
			if 'user' in cherrypy.session:
				# User already logged in - we are adding account
				try: 
					cherrypy.session.get('user').link(account, FindFacebookFriends)
					messaging.send('Success: your Facebook account is now linked.', messaging.SUCCESS)
				except AlreadyLinked: 
					messaging.send("This Facebook account is already connected to another user.", messaging.ERROR)

			else:
				# User not logged in but may already exist in the DB:
				user = cherrypy.session['user'] = User.from_link(account, FindFacebookFriends, name=fbdetails['name'], email=fbdetails['email'])

				# Add recent login status FIXME(abstract?)
				db = session()
				db.add(user)
				user.last_login = datetime.now()
				db.commit()

			raise cherrypy.HTTPRedirect(cherrypy.session.get('redirect', '/'))
		
		else:
			# We do not have a valid redirect.
			if error_reason == "user_denied": messaging.send('Did you press the right button? Facebook claims you cancelled.', messaging.ERROR)
			else: messaging.send("Sorry, a miscellaneous error occured: %s" % error_description, messaging.ERROR)
			raise cherrypy.HTTPRedirect(cherrypy.session.get('redirect', '/') if 'user' in cherrypy.session else '/', 307)
