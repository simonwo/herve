import xanf
from functools import partial
from itertools import chain
from os import path, listdir
from copy import copy

fragments = dict()

scope = dict()
scope.update(xanf.html5tags)

def rmext(f):
    return path.splitext(path.basename(f))[0]

def register(name, callable):
    fragments[name] = callable
    scope.update(fragments)

def load(filename, name=None):
    name = name or rmext(filename)
    with open(filename) as f:
       fragments[name] = partial(render, compile(f.read(), filename, 'eval'))
    scope.update(fragments)

def loadall(dirname, ext='.xanf'):
    for file in listdir(dirname):
        if path.splitext(file)[1] == ext:
            load(path.join(dirname, file))

def get(name):
    return fragments[name] if name in fragments else xanf.html5tags[name]

def addtoscope(**kwargs):
    scope.update(kwargs)

def getscope():
    return copy(scope)

def render(fragment, *args, **kwargs):
    scope = getscope()
    scope['children'] = [a for a in args if type(a) is not dict]
    scope.update(chain(*[d.items() for d in args if type(d) is dict]))
    scope.update(kwargs)
    e = eval(fragment, scope)
    return e

def require(*args):
    return map(lambda arg: get(arg), args)
