from functools import partial
from lxml.etree import Element, iselement

def E(tagname, *args, **kwargs):
    e = Element(tagname)
    for arg in args:
        if iselement(arg):
            e.append(arg)
        elif isinstance(arg, dict):
            e.attrib.update([(k,unicode(v)) for k,v in arg.items()])
        else:
            if len(e) > 0:
                 e[-1].tail = unicode(arg) if e[-1].tail is None else e[-1].tail + unicode(arg)
            else:
                 e.text = unicode(arg) if e.text is None else e.text + unicode(arg)
    e.attrib.update([(k,unicode(v)) for k,v in kwargs.items()])
    return e

def MakeTagBuilder(tagname):
    return partial(E, tagname)

html5tags = dict()
for tag in ['a','abbr','address','area','article','aside','audio','b','base','bb','bdi','bdo','blockquote','body','br','button','canvas','caption','cite','code','col','colgroup','command','data','datagrid','datalist','dd','del','details','dfn','div','dl','dt','em','embed','eventsource','fieldset','figcaption','figure','footer','form','h1','h2','h3','h4','h5','h6','head','header','hgroup','hr','html','i','iframe','img','input','ins','kbd','keygen','label','legend','li','link','mark','map','menu','meta','meter','nav','noscript','object','ol','optgroup','option','output','p','param','pre','progress','q','ruby','rp','rt','s','samp','script','section','select','small','source','span','strong','style','sub','summary','sup','table','tbody','td','textarea','tfoot','th','thead','time','title','tr','track','u','ul','var','video','wbr']:
    html5tags[tag] = MakeTagBuilder(tag)
