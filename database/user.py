from sqlalchemy import Column, String, Unicode, Enum, Integer, DateTime, ForeignKey
from sqlalchemy import and_
from sqlalchemy.orm import relationship as Relationship
from datetime import datetime
from lolevel import session, Base

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode, nullable=False)
    email = Column(Unicode, nullable=False)
    linked_accounts = Relationship('LinkedAccount')
    connected = Relationship('User', secondary=lambda: connected_users, 
                   foreign_keys=lambda: connected_users.columns.userid)
    subscriptions = Relationship('Place', secondary=lambda: subscriptions,
                   primaryjoin="User.id==subscriptions.columns.userid")
    joined = Column(DateTime, default=datetime.now)
    last_login = Column(DateTime)
    offered = Relationship('Reservation', foreign_keys="Reservation.offeredby")
    claimed = Relationship('Reservation', foreign_keys="Reservation.claimedby")

    def __repr__(self):
        return "<User %s '%s' '%s'>" % (self.id, self.name, self.email)

    @staticmethod
    def get(*args): 
        '''Find users which match the given criteria'''
        return session().query(User).join(LinkedAccount).filter(and_(*args))

    @staticmethod
    def exists(*args):
        '''Returns true if any users matched the criteria passed by args'''
        return User.get(*args).count() > 0

    def link(self, account, friendfinder):
        '''Link an externally provided handle to an internal user account'''

        # Check this user isn't already linked 
        db = session()
        if User.exists(LinkedAccount.handle == account.handle):
            raise AlreadyLinked("This account is already attached to another user.")
        db.add(self)

        # Add the deets
        self.linked_accounts.append(account)

        # Connect this user with all their friends
        friends = friendfinder(account.handle, account.token).all()
        self.connected.extend(friends)
        db.commit()

        # Add connections from friends to this user
        User.add_backrefs(self, friends)

    def add_backrefs(user, connections):
        '''Add links from all a user's connections back to this user'''
        db = session()
        db.add_all(connections) # Is this safe? Could it overwrite other actions happening elsewhere?
        for u in connections: # Yes it should be safe, as long as we don't change any other details
            u.connected.append(user)
        db.commit() 

    @staticmethod
    def from_link(account, friendfinder, **kwargs):
        '''Finds or creates a user that matches the linked account'''

        # Discover if the user is already in the db
        matches = User.get(LinkedAccount.handle == account.handle)
        if matches.count() > 0:
            # User is in the DB, return them
            return matches.one()

        else:
            # User not in the db, create them
            db = session()
            user = User(**kwargs)
            db.add(user)
            user.link(account, friendfinder)
            db.commit() #?
            return user



class AlreadyLinked(Exception):
    '''Fired if we try to link an external account that already exists'''
    pass

from sqlalchemy import Table    
connected_users = Table('connected_users', Base.metadata,
    Column('userid', Integer, ForeignKey('users.id')),
    Column('connectedid', Integer, ForeignKey('users.id')))

subscriptions = Table('subscriptions', Base.metadata,
    Column('userid', Integer, ForeignKey('users.id')),
    Column('placeid', String, ForeignKey('places.id')))

class LinkedAccount(Base):
    __tablename__ = 'accounts'
    
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('users.id'))
    source = Column(Enum('facebook', 'twitter', name='SOURCES'))
    handle = Column(Unicode)
    token = Column(String)
    token_expires = Column(DateTime)
    token_refresh = Column(String)
    linked = Column(DateTime, default=datetime.now)
    updated = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    
    def __repr__(self):
        return "<LinkedAccount %s for User %s (%s)>" % (self.id, self.userid, self.source)

