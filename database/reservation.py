from lolevel import Base, session, engine
from sqlalchemy import Column, Text, String, DateTime, SmallInteger, Unicode, Integer, ForeignKey
from sqlalchemy.exc import IntegrityError
from datetime import datetime as dt

class Reservation(Base):
    __tablename__ = 'reservations'

    id = Column(Integer, primary_key=True)
    place = Column(String, ForeignKey('places.id'))
    datetime = Column(DateTime, nullable=False)
    people = Column(SmallInteger, nullable=False)
    bookingref = Column(Unicode, nullable=False)
    extra = Column(Text)
    unlist = Column(DateTime)
    offered = Column(DateTime, default=dt.now)
    offeredby = Column(Integer, ForeignKey('users.id'))
    claimed = Column(DateTime)
    claimedby = Column(Integer, ForeignKey('users.id'))

    def __repr__(self):
        return "<Reservation %s for Place %s at %s>" % (self.id, self.place, self.datetime)

    def claim(self, user):
	'''Claims the reservation for the specified 
	user. May generate an exception that should
	be handled!'''

    	db = session()
	
	# Check that it's not already claimed
	if self.claimedby is not None: raise AlreadyClaimed()
    	
	# Perform the claim
	# Note this (might?) induce a race
	# condition so a trigger which enforces
	# this is included in the db schema
	try:
		db.add(self)
    		self.claimed = datetime.now()
	    	self.claimedby = user
    		db.commit()
	except IntegrityError:
		db.rollback()
		raise AlreadyClaimed()

    @staticmethod
    def by_place(place):
    	return session().query(Reservation).join(Place).filter(Place.id == place.id)

class AlreadyClaimed(Exception): pass


