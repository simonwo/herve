from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

from sqlalchemy import create_engine
from os import path
dbpath = path.join(path.dirname(__file__), '../run/sqlite.db')
engine = create_engine('sqlite:///' + dbpath)

from sqlalchemy.orm import sessionmaker, scoped_session
from functools import partial
session = scoped_session(sessionmaker(bind=engine))