from sqlalchemy import Column, String, Float, Unicode, Enum, SmallInteger
from sqlalchemy.orm import relationship as Relationship
from googleplaces import types
from lolevel import Base, session
from datetime import datetime
from urllib import quote_plus
from user import subscriptions
 
class Place(Base):
    '''A local copy of all place information
    which reservations are linked against.'''
    
    __tablename__ = 'places'

    id = Column(String, primary_key=True)
    name = Column(Unicode, nullable=False)
    slug = Column(String)
    images = Relationship("Image")
    reservations = Relationship("Reservation")
    address = Column(Unicode)
    phone = Column(Unicode)
    price = Column(SmallInteger)
    latitude = Column(Float)
    longitude = Column(Float)
    types = Relationship("PlaceType")
    subscribed = Relationship("User", secondary=subscriptions,
                            primaryjoin="Place.id==subscriptions.columns.placeid")

    def __repr__(self):
        return "<Place %s '%s'>" % (self.id, self.fullname)

    @property
    def fullname(place):
        return "%s, %s" % (place.name, place.address)

    @property
    def slug_url(place):
        return quote_plus(place.slug)

    def generate_slug(place):
        '''Generate a unique URL segment for this place'''
        db = session()
        slug_exists = lambda s: db.query(Place).filter(Place.slug == s).count() > 0
        slug = ''

        # Try using just the name, then plus the first line of address, etc
        for part in place.fullname.split(','):
            slug += part.lower().encode('utf-8')
            if not slug_exists(slug): return slug

        # Failed so far, so add parts of the ID until we get something unique
        slug += "+"
        for char in place.id:
            slug += char
            if not slug_exists(slug): return slug

        # Panic and freak out
        raise Exception("Failed to generate a unique slug for Place %s", str(place))

    @staticmethod
    def find_by_slug(slug):
        '''Find a place by it's unique slug (with urlencoding removed)'''
        return session().query(Place).filter(Place.slug == slug)

def BuildFromGooglePlace(gplace):
    place = Place()
    place.id = gplace.id
    place.name = gplace.name
    place.address = gplace.formatted_address
    place.phone = gplace.local_phone_number
    #place.price = gplace.price
    place.latitude = gplace.geo_location['lat']
    place.longitude = gplace.geo_location['lng']
    place.types = [PlaceType(placeid=gplace.id, type=t) for t in gplace.types]
    return place


from sqlalchemy import ForeignKey, Integer
class PlaceType(Base):
    '''As each place can have multiple types, this
    table lists the types associated with each place.'''
    __tablename__ = 'placetypes'

    id = Column(Integer, primary_key=True, autoincrement=True)
    placeid = Column(String, ForeignKey(Place.id), nullable=False)
    type = Column(Enum(*[getattr(types, type) for type in dir(types) if "TYPE" in type]), nullable=False) 
    
    def __repr__(self):
        return "<Type '%s' on Place %s>" % (self.type, self.placeid)

    def __eq__(self, t):
        return self.type == t

from sqlalchemy import DateTime
from itertools import count
from os import path
class Image(Base):
    '''An image associated with a place'''
    __tablename__ = 'images'

    id = Column(Integer, primary_key=True)
    place = Column(String, ForeignKey('places.id'))
    filename = Column(String)
    width = Column(Integer, nullable=False)
    height = Column(Integer, nullable=False)
    mimetype = Column(String(20))
    created = Column(DateTime, default=datetime.now)

    def __repr__(self):
        return "<Image %s for Place %s '%s'>" % (self.id, self.place, self.filename) 

    @staticmethod
    def generate_filename(unique, ext, directory):
        filetemplate = unique + "-%d" + ext
        counter = count(1)

        filename = unique + ext
        while path.exists(path.join(directory, filename)):
            filename = filetemplate % counter.next()
        return filename