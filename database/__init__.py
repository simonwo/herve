from lolevel import Base, session, engine
from place import *
from user import *
from reservation import *

Base.metadata.create_all(engine)

# Add a trigger to prevent reservation claiming race conditions
single_claim_trigger = """CREATE TRIGGER IF NOT EXISTS ClaimOnce BEFORE UPDATE OF claimed, claimedby ON reservations BEGIN
    SELECT CASE WHEN OLD.claimedby IS NOT NULL THEN RAISE(FAIL, 'Already claimed.') END;
END;"""
engine.execute(single_claim_trigger)
